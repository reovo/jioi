<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/apidocs', function () {
    return \File::get(public_path() . '/docs/index.html');
});

//catch all
Route::any('/{any}', function () {
    error_log("'catch all' route!");
    return \File::get(public_path() . '/index.html');
})->where('any', '.*');
