<?php

namespace App\Http\Controllers;

use App\UserApplication; //the model
use App\Http\Resources\UserApplicationResource; //the API detail view
use App\Http\Resources\UserApplicationsResource; //the API collection view
use App\Http\Resources\ScanLogsResource; //the API collection view
use App\Http\Requests\StoreUserApplicationRequest; //the validator
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;

class UserApplicationController extends Controller{
    public function __construct(){
        $this->middleware('auth.basic.once');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserApplication  $application
     * @return \Illuminate\Http\Response
     */
    public function scanlogs(Request $request){
        if(Auth::user()->role != 'S'){
            return response("{'error': 'unauthorized'}", 401);
        }
        
        //see if there's an activity with that code by the same application
        $scans = Activity::inLog('scans')->get();
        return response(new ScanLogsResource($scans));
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loggedUser = Auth::user();
        //S: superadmin, A: admin, U: user
        //If logged in user is superadmin
        if($loggedUser->role == 'S' || $loggedUser->role == 'G'){
            //superadmin can view all applications
            $applications = UserApplication::all();
        }
        if($loggedUser->role == 'G'){
            //guard can only view approved applications
            $applications = UserApplication::where('approved', true)->get();
        }
        else if($loggedUser->role == 'A'){
            //admin can only view applications from their country
            $applications = UserApplication::where('country', $loggedUser->country)->get();
        }

        // Return a collection of $applications
        return response(new UserApplicationsResource($applications));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserApplicationRequest $request)
    {
        // $payload = json_decode($request->getContent(), true);
        error_log("Store");
        if(Auth::user()->role != 'S' && Auth::user()->role != 'A'){
            return response("{'error': 'unauthorized'}", 401);
        }
        //athletes must have discipline
        if(strtolower($request->input('status')) == "athlete" && $request->input("discipline") == null){
            return response("{'error': 'Discipline missing'}", 422);
        }
        // $application = $request->isMethod('put') ? UserApplication::findOrFail($request->id) : new UserApplication;
        $application = new UserApplication;
        
        $application->fill($request->all());
        $application->country = strtolower($request->input('country'));
        // $application->name = $request->input('name');
        // $application->last_name = $request->input('last_name');
        // $application->photo = $request->file('photo')->store('avatars');
        $application->addMediaFromRequest('photo')->toMediaCollection('avatars');
        $application->addMediaFromRequest('passport')->toMediaCollection('passports');
        // $application->photo_crop_x = $request->input('photo_crop_x');
        // $application->photo_crop_y = $request->input('photo_crop_y');
        // $application->photo_crop_width = $request->input('photo_crop_width');
        // $application->photo_crop_height = $request->input('photo_crop_height');
        $application->dob = date_create_from_format('Y-m-d', $request->input('dob'));
        // $application->gender = $request->input('gender');
        $application->nationality = strtolower($request->input('nationality'));
        // $application->passport_no = $request->input('passport_no');
        $application->passport_exp = date_create_from_format('Y-m-d', $request->input('passport_exp'));
        $application->status = strtolower($request->input('status'));
        $application->discipline = strtolower($request->input('discipline'));
        $application->approved = false;
        // $application->date_approved = date_create_from_format('Y-m-d', $request->input('date_approved'));
        
        if($application->save()) {
            return response(new UserApplicationResource($application));
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserApplication  $application
     * @return \Illuminate\Http\Response
     */
    public function show(UserApplication $application)
    {
        if(Auth::user()->role == 'A' && Auth::user()->country != $application->country){
            return response("{'error': 'unauthorized'}", 401);
        }
        if(Auth::user()->role == 'G' && !$application->approved){
            return response("{'error': 'unauthorized'}", 401);
        }

        return response(new UserApplicationResource($application));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserApplication  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserApplication $application){
        //embed scan functionality
        if($request->input('scan') && $request->has('code')){
            if(Auth::user()->role != 'S' && Auth::user()->role == 'G'){
                return response("{'error': 'unauthorized'}", 401);
            }
            if(!$application->approved){
                return response("{'error': 'unauthorized'}", 401);
            }
            
            //see if there's an activity with that code by the same application
            $match = Activity::inLog('scans')->where('description', $request->input('code'))->first();
            if($match){
                return response(new UserApplicationResource($application));
            }
            else{
                //log scan
                activity('scans')
                ->performedOn($application)
                ->causedBy(Auth::user())
                ->log($request->input('code'));
                return response(new UserApplicationResource($application));
            }
        }

        //actual update
        if(Auth::user()->role != 'S' && Auth::user()->role != 'A'){
            return response("{'error': 'unauthorized'}", 401);
        }
        if(Auth::user()->role == 'A' && Auth::user()->country != $application->country){
            return response("{'error': 'unauthorized'}", 401);
        }
        error_log("Update");
        //athletes must have discipline
        if($request->input('status') == "athlete" && $request->input("discipline") == null){
            return response("{'error': 'Discipline missing'}", 422);
        }
        $wasApproved = $application->approved;
        $country = $application->country;

        $application->fill($request->all());
        $application->country = strtolower($request->input('country', $application->country));
        $application->nationality = strtolower($request->input('nationality', $application->nationality));
        if($request->has('dob')){
            $application->dob = date_create_from_format('Y-m-d', $request->input('dob'));
        }
        if($request->has('passport_exp')){
            error_log("passport exp");
            $application->passport_exp = date_create_from_format('Y-m-d', $request->input('passport_exp'));
        }
        $application->status = strtolower($request->input('status', $application->status));
        $application->discipline = strtolower($request->input('discipline', $application->discipline));
        if($request->has('photo')){
            error_log("photo");
            // $application->deleteMedia();
            $application->addMediaFromRequest('photo')->toMediaCollection('avatars');
        }
        if($request->has('passport')){
            error_log("passport");
            // $application->deleteMedia();
            $application->addMediaFromRequest('passport')->toMediaCollection('passports');
        }
        if($request->has('approved')){
            if($request->input('approved') == true){
                $application->approved = true;
                if(!$wasApproved){
                    $application->date_approved = date('Y-m-d');
                }
            }
            else if($request->input('approved') == false){
                //application has been unapproved
                $application->approved = false;
                $application->date_approved = null;
            }
        }
        
        if($application->save()) {
            return response(new UserApplicationResource($application));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserApplication  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserApplication $application)
    {
        if(Auth::user()->role != 'S' && Auth::user()->role != 'A'){
            return response("{'error': 'unauthorized'}", 401);
        }
        if(Auth::user()->role == 'A' && Auth::user()->country != $application->country){
            return response("{'error': 'unauthorized'}", 401);
        }
        if($application->delete()) {
            return response(new UserApplicationResource($application));
        }
    }
}
