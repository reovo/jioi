<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $photo = null;
        if($this->getMedia('avatars')->first() != null){
            $photo = $this->getMedia('avatars')->first()->getUrl();
        }
        $passport = null;
        if($this->getMedia('passports')->first() != null){
            $passport = $this->getMedia('passports')->first()->getUrl();
        }
        return [
            'id'            => $this->id,
            'country' => (string)$this->country,
            'last_name' => (string)$this->last_name,
            'name' => (string)$this->name,
            'photo' => $photo,
            'photo_crop_x' => (float)$this->photo_crop_x,
            'photo_crop_y' => (float)$this->photo_crop_y,
            'photo_crop_width' => (float)$this->photo_crop_width,
            'photo_crop_height' => (float)$this->photo_crop_height,
            'dob' => $this->dob,
            'gender' => $this->gender,
            'nationality' => (string)$this->nationality,
            'passport' => $passport,
            'passport_no' => (string)$this->passport_no,
            'passport_exp' => $this->passport_exp,
            'status' => $this->status,
            'discipline' => $this->discipline,
            'approved' => $this->approved,
            'date_approved' => $this->date_approved,
            'created_at' => $this->date_approved,
            'date_approved' => $this->date_approved,
        ];
        // return parent::toArray($request);
    }
}
