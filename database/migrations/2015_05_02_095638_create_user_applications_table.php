<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country');
            $table->string('name');
            $table->string('last_name');
            $table->float('photo_crop_x')->default(0);
            $table->float('photo_crop_y')->default(0);
            $table->float('photo_crop_width')->default(0);
            $table->float('photo_crop_height')->default(0);
            $table->date('dob');
            $table->char('gender', 1); //Male: 'M', Female: 'F'
            $table->string('nationality'); //use iso country codes
            $table->string('passport_no')->unique();
            $table->date('passport_exp')->nullable();
            $table->string('status');
            $table->string('discipline')->nullable();
            $table->boolean('approved');
            $table->string('date_approved')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_applications');
    }
}
