User
==================
superadmin has all access
admins can perform all operations ONLY on users from the same country
admins have no access to other admins
users have access to self only
anons have no access

User Applications
==================
superadmin has all access
admin can perform all operations ONLY on applications from the same country
anon can only create