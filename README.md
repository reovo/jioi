## Configure Project
Copy `.env` file into project root. It is not included for security reasons. It contains DB password.

## Start Server
Run the following command from the project root
`php artisan serve`

## Refresh Database
`php artisan migrate:refresh`

## Populate Database with Mock Data
Users table
`php artisan db:seed --class=UsersTableSeeder`

User applications table
`php artisan db:seed --class=UserApplicationsTableSeeder`

# API Usage for Mobile App
The `/apidocs` route provides valuable information about the models used in the app. The most important ones are the `User` and `Application` models. `User` represents an entity that is allowed to log into the site, such as an admin or a security officer. `Application` represents an accreditation application. The structures of the models can be seen on `/apidocs`.

## API Authentication
If using a webview, the login page on the site can be used to log a user in. Authentication token is stored in localStorage and can be used by the webview subsequently to access other frontend links such as the accreditation profile.
If a custom Authentication page is needed for the mobile app or if direct access to API routes is needed, authentication on should be done with Basic Authentication. That means every request made to the API should have an `Authorization` header whose value is in the form `Basic <AUTH_TOKEN>`. The token can be computed by encoding the user's `email:password` to base64.
Also, sending a POST request to `/api/login` containing a `username` field and `password` field in the request body can be used to verify login information. A 200 status is returned if the `username` and `password` are correct along with a 
json object representing the authenticated user. A 401 status is returned if the information is incorrect.

## Scan QR Code
The QR code contains a link to the accreditation profile in the format `/accreds/{id}/scan`. If a webview was used to log the user in, then this link can be used to conveniently display the accreditation profile. The page has been optimized for 
mobile screens.
However, if a custom authentication page is used to log the user in, the user's authentication token should be stored by the app and provided in the `Authorization` header for all future requests. To get the accreditation profile data, a GET request should be sent to `/api/applications/{id}`. If authentication is successful, a 200 status is returned along with the accreditation profile data. Otherwise, a 401 status is returned.

## Displaying Photo
The uploaded pictures are not cropped before being stored. Instead cropping transformations are stored in the accreditation profile data. This is to allow admins to make adjustments to the photo zoom and crop if needed. As such, when displaying the photo, some math is needed.
The `x` and `y` values are stored as percentages of the image width and height respectively. The `width` and `height` values represent how much of the image is shown, in other words, the zoom level. Aspect ratio is `35:45` according to the requirements. With all that in mind, here's an example of how it is displayed on the website.
``` javascript
//calc top position
let t = application.photo_crop_y * fullImgHeight;
//calc css right position
let r = fullImgWidth - ((application.photo_crop_width + application.photo_crop_x) * fullImgWidth);
//calc css bottom position
let b = fullImgHeight - ((application.photo_crop_height + application.photo_crop_y) * fullImgHeight);
//calc left position
let l = application.photo_crop_x * fullImgWidth;
//scale of image display
const scale = 4;
return (
  <div style={{
    position: 'relative',
    overflow: 'hidden',
    width: 35 * scale, //enforce aspect ratio
    height: 45 * scale,
  }}>
    <div style={{
      transformOrigin: 'top left',
      transform: `scale(${1 / ((application.photo_crop_width * fullImgWidth) / (35 * scale))})`, //apply zoom
    }}>
      <img
        src={application.photo}
        style={{
          position: 'absolute',
          clipPath: `inset(${t.toFixed(0)}px ${r.toFixed(0)}px ${b.toFixed(0)}px ${l.toFixed(0)}px)`, //crop image
          left: application.photo_crop_x * fullImgWidth * -1, //position image
          top: application.photo_crop_y * fullImgHeight * -1,
        }} />
    </div>
  </div>
);
```